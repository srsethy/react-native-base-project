import React from 'react';
import {ActivityIndicator, StatusBar, View, StyleSheet} from 'react-native';

export class AppNavigator extends React.Component {
  constructor(props: Readonly<{}>) {
    super(props);
    this._bootstrapAsync();
  }
  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    // const userToken = await _retrieveData(asyncStorage.token);
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    /* Network.setupNetworkConfig()
      .then(config => {
        Network.setupIntercepter();
        this.props.navigation.navigate(
          !!(config.token) ? 'App' : 'Auth',
        );
      })
      .catch(error => this.props.navigation.navigate('Auth'));*/

    this.props.navigation.navigate('Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="default" />
        <ActivityIndicator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
