import React, {useState, useEffect} from 'react';

import {SafeAreaView, ScrollView, Modal, StatusBar} from 'react-native';
import {NetworkStatus} from '../../widgets/NetInfo';
import {Loader} from '../../widgets/Loader';
import {COLORS} from '../../utils/Colors/Colors';
import CustomText from '../pureComponents/Text';
import {ModalHoc} from '../pureComponents/ModalsHoc';
export const AppWrapper = Component => props => {
  const [isLoading, setLoading] = useState(false);
  const [showModal, toggleModal] = useState(false);
  useEffect(() => {}, [isLoading, showModal]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar translucent backgroundColor="red" barStyle="dark-content" />
      <NetworkStatus />
      <ScrollView
        overScrollMode={'always'}
        style={[{backgroundColor: COLORS.bgColor, flex: 1}, props.contentStyle]}
        contentContainerStyle={{flexGrow: 1}}>
        <Component {...props} isLoading={isLoading} setLoading={setLoading} />
      </ScrollView>
      {isLoading && <Loader />}
      <ModalHoc showModal={showModal} toggleModal={toggleModal} />
    </SafeAreaView>
  );
};
