interface IimageObj {
  image: any;
}
interface IStaticImagesAssetImport {
  [key: string]: IimageObj;
}
const StaticImagesAssetImport: IStaticImagesAssetImport = {
  paramIcon: {
    image: require('../../../assets/icons/param.png'),
  },
};

export default StaticImagesAssetImport;
