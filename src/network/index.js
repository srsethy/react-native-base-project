import axiosInstance from './axios/axiosInstance';
import {setupIntercepter} from './axios/interceptor';
import {_retrieveData, _storeData} from '../storage';
import {asyncStorage} from '../storage/Type';
import Snackbar from 'react-native-snackbar';
import {API} from './Api';
import {COLORS} from '../utils/Colors/Colors';

const setAuthToken = (hash: string) => {
  axiosInstance.defaults.headers.common['Authorization'] = `Basic ${hash}`;
  global.token = hash;
  _storeData(asyncStorage.token, hash);
};

const setupNetworkConfig = async () => {
  return new Promise(async (resolve, reject) => {
    // Override timeout default for the library
    // Now all requests using this instance will wait 2.5 seconds before timing out
    axiosInstance.defaults.timeout = 2500;
    let token = await _retrieveData(asyncStorage.token);
    if (token) {
      global.token = token;
      axiosInstance.defaults.headers.common['Authorization'] = `Basic ${token}`;
      resolve({token});
    } else reject();
  });
};

const handleError = (error: {
  response: {data: any},
  request: any,
  message: any,
  config: any,
}) => {
  if (error.message && error.message === 'Network Error') {
    Snackbar.show({
      title: error.message,
      duration: Snackbar.LENGTH_SHORT,
      backgroundColor: COLORS.black,
    });
  } else if (error.response) {
    Snackbar.show({
      title: error.response.data.message,
      duration: Snackbar.LENGTH_SHORT,
      backgroundColor: COLORS.black,
    });
    return error.response;
  }
  /*
  if (error.request) {
    console.warn('Error Req->', error.request);
    Snackbar.show({
      title: error.request,
      duration: Snackbar.LENGTH_SHORT,
      backgroundColor: COLORS.black,
    });
    return;
  } else {
    console.warn('Error Req->', error.message);
    Snackbar.show({
      title: error.message,
      duration: Snackbar.LENGTH_SHORT,
      backgroundColor: COLORS.black,
    });
    return;
  }*/
};

const login = (username: string, password: string, domain: string) => {
  let url = API.login;
  setAuthToken(username, password, domain);
  return axiosInstance
    .post(url, {
      username,
      password,
    })
    .catch(error => handleError(error));
};

export const Network = {
  setupNetworkConfig,
  setupIntercepter,
  login,
};
