import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import TextView from '../../components/pureComponents/Text';
import Input from '../../components/pureComponents/Input';
import {mS, mVs, s} from '../../widgets/ResponsiveScreen';
import Button from '../../components/pureComponents/Button';
import {IconRightChevron} from '../../assets/';
import {_storeData} from '../../storage';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import {COLORS} from '../../utils/Colors/Colors';
import {AppWrapper} from '../../components/hoc/AppWrapper';
import {shadow} from '../../utils/Colors/Shadow';
import {Network} from '../../network';
import {asyncStorage} from '../../storage/Type';

const _renderIcon = <IconRightChevron height={mVs(10)} width={s(10)} />;

@observer
class Login extends Component {
  @observable userName = 'sri_charan';
  @observable password = 'licious@123';
  @observable loginError;
  @observable userError = {};
  @observable passwordError = {};
  @observable userNamePlaceholder = 'Username';
  @observable passwordPlaceholder = 'Password';
  @observable isLoading = false;
  _login = () => {
    if (!this.userName) {
      this.userError = {borderColor: COLORS.red, borderWidth: 1};
      this.userNamePlaceholder = 'Please enter your username';
      return;
    } else if (!this.password) {
      this.passwordError = {
        borderColor: COLORS.red,
        borderWidth: 1,
      };
      this.passwordPlaceholder = 'Please enter your password';
      return;
    }
    this.props.setLoading(true);
    Network.login(this.userName, this.password).then(async response => {
      this.props.setLoading(false);
      if (response.status === 200) {
        await _storeData(asyncStorage.refresh, response.data.data.refresh);
        /* _storeData(asyncStorage.access, response.data.access);
                _storeData(asyncStorage.id, response.data.id);
                _storeData(asyncStorage.username, response.data.username);
                _storeData(asyncStorage.email, response.data.email);
                _storeData(asyncStorage.first_name, response.data.first_name);
                _storeData(asyncStorage.first_name, response.data.first_name);
                _storeData(asyncStorage.last_name, response.data.last_name);
                _storeData(
                  asyncStorage.is_staff,
                  response.data.is_staff ? 'true' : 'false',
                );
                _storeData(asyncStorage.date_joined, response.data.date_joined);
                _storeData(asyncStorage.groups, response.data.groups.join());*/
        this.props.navigation.navigate('Home');
      }
    });
  };

  render() {
    return (
      <View style={{padding: mS(10), flex: 1}}>
        <TextView>Hello Login</TextView>

        <Input
          wapperStyle={{
            ...this.userError,
            fontSize: mS(20),
            marginTop: mS(25),
          }}
          onChangeText={userName => (this.userName = userName.trim())}
          value={this.userName}
          placeholder={this.userNamePlaceholder}
          inputStyle={{flex: 1}}
        />
        <Input
          secureTextEntry={true}
          wapperStyle={{
            ...this.passwordError,
            fontSize: mS(20),
            marginTop: mS(25),
          }}
          onChangeText={password => (this.password = password)}
          value={this.password}
          placeholder={this.passwordPlaceholder}
          inputStyle={{flex: 1}}
        />
        <Button
          style={[shadow, {marginTop: mS(30)}]}
          title={'Login'}
          onPress={this._login}
          iconRight={_renderIcon}
        />
      </View>
    );
  }
}

Login.propTypes = {};

export default AppWrapper(Login);
