import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, Text, SafeAreaView} from 'react-native';
import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';

@inject('counterStore')
@observer
class Home extends Component {
  @observable userName = 'Soumya';
  componentDidMount(): void {
    this.props.counterStore.setCount(100);
    this.props.counterStore.increment();

    this.userName = 'Pranjal';
  }

  render() {
    return (
      <SafeAreaView>
        <View>
          <Text>Hello Home {this.userName}</Text>
        </View>
      </SafeAreaView>
    );
  }
}

Home.propTypes = {};

export default Home;
