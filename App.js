import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';
import {YellowBox, SafeAreaView} from 'react-native';
import {AppContainer} from './src/navigator/AppContainer';
import {Provider} from 'mobx-react';
import {stores} from './src/stores';

@observer
class App extends Component {
  componentDidMount(): void {
    YellowBox.ignoreWarnings([
      'Warning: componentWill',
      'Warning: Async Storage',
      'Warning: Each',
    ]);
  }

  render() {
    return (
      <Provider {...stores}>
        <AppContainer />
      </Provider>
    );
  }
}

App.propTypes = {};

export default App;
